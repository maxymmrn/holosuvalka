import 'package:flutter/material.dart';
import 'package:holosuvalka/model/election/election.dart';
import 'package:holosuvalka/style/color.dart';

extension ElectionTypeX on ElectionType {
  Color get color {
    switch (this) {
      case ElectionType.local:
        return AppColors.localBackground;
      case ElectionType.national:
        return AppColors.nationalBackground;
      case ElectionType.organisation:
        return AppColors.organizationBackground;
      default:
        return AppColors.lightGray;
    }
  }

  String get textType{
    switch(this){
      case ElectionType.local:
        return 'Місцеві вибори';
      case ElectionType.national:
        return 'Державні вибори';
      case ElectionType.organisation:
        return 'Вибори до організації';
      default:
        return 'Вибори до організації';
    }
  }
}