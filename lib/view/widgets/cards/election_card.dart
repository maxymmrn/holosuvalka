import 'package:flutter/material.dart';
import 'package:holosuvalka/model/election/election.dart';
import 'package:flutter/cupertino.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/view/ext/election_type_ext.dart';
import 'package:intl/intl.dart';

class ElectionCard extends StatelessWidget {
  final Election election;
  final VoidCallback onClick;

  const ElectionCard({
    required this.election,
    required this.onClick,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
        height: 76,
        margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: election.type.color,
          boxShadow: [
            BoxShadow(
              color: election.type.color,
              blurRadius: 4,
              offset: const Offset(4, 6),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 13, 12, 13),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width - 148,
                    child: Text(
                      election.questionnaire.name,
                      style: AppTextStyles.semiBold18(AppColors.darkGray),
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Text(
                    election.type.textType,
                    style: AppTextStyles.semiBold12(AppColors.mediumGray),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Container(
                  width: 80,
                  child: Text(
                    'Доступно до '
                    '${DateFormat('yyyy-MM-dd').format(election.endDatetime)}',
                    style: AppTextStyles.semiBold10(AppColors.darkGray),
                    textAlign: TextAlign.right,
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
