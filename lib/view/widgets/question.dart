import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:holosuvalka/model/election/election.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/controller/bloc/vote_cubit.dart';
import 'package:holosuvalka/view/widgets/text_input_fields/standard.dart';

class QuestionTile extends StatelessWidget {
  final Question question;

  const QuestionTile(this.question) : super();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 24),
      child: Column(children: [_buildHeader(), ..._buildStrategy(context)]),
    );
  }

  Widget _buildHeader() {
    return Container(
      alignment: Alignment.centerLeft,
      height: 56,
      padding: const EdgeInsets.only(left: 20),
      decoration: BoxDecoration(
        color: AppColors.background.withOpacity(0.5),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
      ),
      child: Text(
        question.description,
        style: AppTextStyles.regular20(AppColors.darkGray),
      ),
    );
  }

  List<Widget> _buildStrategy(BuildContext context) {
    switch (question.questionType) {
      case QuestionType.radioItem:
        return question.choices
            .map((choice) => _buildRadioAnswer(context, choice))
            .toList();
      case QuestionType.checkbox:
        return question.choices
            .map((choice) => _buildCheckboxAnswer(context, choice))
            .toList();
      case QuestionType.distribution:
        return question.choices
            .map((choice) => TextInputField(label: choice.answer.toString()))
            .toList();
    }
  }

  Widget _buildRadioAnswer(BuildContext context, Choice choice) {
    return Container(
      color: AppColors.background,
      margin: const EdgeInsets.symmetric(vertical: 1),
      child: RadioListTile<Choice>(
        value: choice,
        groupValue: question.choices.singleWhereOrNull(
          (e) => context.read<VoteCubit>().getValue(e.id)! > 0,
        ),
        title: Text(
          '${choice.answer}',
          style: AppTextStyles.semiBold14(AppColors.mediumGray),
        ),
        onChanged: (choice) => context.read<VoteCubit>().switchRadioValue(
              choice?.id ?? 0,
              question.choices.map((choice) => choice.id).toList(),
            ),
      ),
    );
  }

  Widget _buildCheckboxAnswer(BuildContext context, Choice choice) {
    return Row(
      children: [
        Checkbox(
          value: context.read<VoteCubit>().getValue(choice.id)! > 0,
          onChanged: (_) =>
              context.read<VoteCubit>().switchCheckboxValue(choice.id),
        ),
        const SizedBox(width: 16),
        Text(
          '${choice.answer}',
          style: AppTextStyles.semiBold14(AppColors.mediumGray),
        ),
      ],
    );
  }
}
