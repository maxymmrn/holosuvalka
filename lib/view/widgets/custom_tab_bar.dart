import 'package:flutter/material.dart';
import 'package:holosuvalka/style/color.dart';

class CustomTabBar extends StatelessWidget implements PreferredSizeWidget {
  final List<String> titles;
  const CustomTabBar(this.titles) : super();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 48,
      backgroundColor: AppColors.background,
      flexibleSpace: TabBar(
        labelColor: AppColors.primary,
        unselectedLabelColor: AppColors.lightGray,
        indicatorColor: AppColors.primary,
        tabs: titles.map((title) => Tab(text: title)).toList(),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(48);
}
