import 'package:flutter/material.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';

class WhiteAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  const WhiteAppBar(this.title) : super(key: null);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: true,
      backgroundColor: AppColors.background,
      iconTheme: IconThemeData(color: AppColors.darkGray),
      title: Text(
        this.title,
        style: AppTextStyles.semiBold18(AppColors.darkGray),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(56);
}
