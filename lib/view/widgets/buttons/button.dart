import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';

class Button extends StatelessWidget {
  final String label;
  final bool filled;
  final VoidCallback? onClick;

  const Button({
    required this.label,
    required this.filled,
    this.onClick,
  }) : super();

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor;
    final Color textColor;
    final double shadowOpacity;

    if (filled) {
      backgroundColor = AppColors.primary;
      textColor = AppColors.background;
      shadowOpacity = 0.25;
    } else {
      backgroundColor = AppColors.background;
      textColor = AppColors.primary;
      shadowOpacity = 0.15;
    }

    return InkWell(
      onTap: onClick,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 48,
        margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Center(
          child: Text(
            label,
            style: AppTextStyles.semiBold18(textColor),
            textAlign: TextAlign.center,
          ),
        ),
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(16)),
          border: const Border.fromBorderSide(
            BorderSide(
              style: BorderStyle.solid,
              color: AppColors.primary,
              width: 0,
            ),
          ),
          boxShadow: [
            BoxShadow(
              color: AppColors.primary.withOpacity(shadowOpacity),
              spreadRadius: 0,
              blurRadius: 4,
              offset: const Offset(0, 4),
            ),
          ],
        ),
      ),
    );
  }
}
