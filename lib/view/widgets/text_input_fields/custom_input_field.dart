import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomInputField extends StatelessWidget {
  final String label;
  final Icon? icon;
  final String? hint;
  final TextInputType? keyboardType;
  final void Function(String)? onChanged;

  const CustomInputField({
    required this.label,
    this.hint,
    this.icon,
    this.keyboardType,
    this.onChanged,
  }) : super();

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        child: TextFormField(
          keyboardType: keyboardType,
          onChanged: onChanged,
          decoration: InputDecoration(
            border: const UnderlineInputBorder(),
            labelText: label,
            hintText: hint,
            suffixIcon: icon,
          ),
        ),
      );
}
