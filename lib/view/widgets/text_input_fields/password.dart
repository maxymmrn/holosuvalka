import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PasswordTextInputField extends StatelessWidget {
  final String? hint;
  final String label;

  const PasswordTextInputField({
    Key? key,
    this.hint,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
        child: TextFormField(
          // controller: _controller,
          decoration: InputDecoration(
            border: UnderlineInputBorder(),
            labelText: label,
            hintText: hint,
            suffixIcon: Icon(
              // onPressed: () => _controller.clear(),
              Icons.remove_red_eye_rounded,
            ),
          ),
        ),
      );
}
