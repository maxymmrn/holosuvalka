import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextInputField extends StatelessWidget {
  final String? hint;
  final String label;
  final TextInputType? keyboardType;

  const TextInputField({
    Key? key,
    this.hint,
    required this.label,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
        child: TextFormField(
          decoration: InputDecoration(
            border: UnderlineInputBorder(),
            labelText: label,
            hintText: hint,
          ),
            keyboardType: keyboardType,
        ),
      );
}
