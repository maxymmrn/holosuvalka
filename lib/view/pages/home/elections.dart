import 'package:flutter/material.dart';
import 'package:holosuvalka/controller/api.dart';
import 'package:holosuvalka/controller/http_result.dart';
import 'package:holosuvalka/model/election/election.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/view/pages/vote/vote.dart';
import 'package:holosuvalka/view/widgets/custom_tab_bar.dart';
import 'package:holosuvalka/view/widgets/cards/election_card.dart';

class ElectionsPage extends StatefulWidget {
  const ElectionsPage() : super();

  @override
  _ElectionsPageState createState() => _ElectionsPageState();
}

class _ElectionsPageState extends State<ElectionsPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: AppColors.background,
        appBar: const CustomTabBar(['Доступні', 'Завершені']),
        body: TabBarView(
          children: [
            _buildElectionsBody(),
            const Text('Second page'),
          ],
        ),
      ),
    );
  }

  Widget _buildElectionsBody() {
    return FutureBuilder<HttpResult<List<Election>>>(
      future: Api.instance.getElections(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.active:
            return const Center(child: Text('Loading...'));
          case ConnectionState.done:
            if (snapshot.hasData) {
              return snapshot.data!.when(
                fail: (status, message) => Center(child: Text('$message')),
                success: (elections) => ListView(
                  children: _buildElections(elections),
                ),
              );
            } else {
              return const Center(child: Text('Snapshot data is null'));
            }
          case ConnectionState.none:
          default:
            return const Center(child: Text('Future is null'));
        }
      },
    );
  }

  List<Widget> _buildElections(elections) {
    final electionsCards = (elections as List)
        .cast<Election>()
        .map((el) => ElectionCard(
              election: el,
              onClick: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => VotePage(el)),
              ),
            ))
        .toList();

    return [const SizedBox(height: 16), ...electionsCards];
  }
}
