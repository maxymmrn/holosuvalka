import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/view/pages/home/elections.dart';
import 'package:holosuvalka/view/pages/home/statistics.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final pages = [ElectionsPage(), StatisticsPage()];
  late String title;
  late PageController controller = PageController(initialPage: 0);
  late int pageIndex;
  double? elevation;

  @override
  void initState() {
    super.initState();
    title = 'Голосування';
    elevation = 0;
    pageIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      appBar: AppBar(
        elevation: elevation,
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.background,
        title: Text(
          title,
          style: AppTextStyles.semiBold18(AppColors.darkGray),
        ),
      ),
      body: PageView.builder(
        controller: controller,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 2,
        itemBuilder: (context, index) => pages[index],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: AppColors.background,
        selectedItemColor: AppColors.primary,
        unselectedItemColor: AppColors.lightGray,
        currentIndex: pageIndex,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/icons/vote.svg',
              color: AppColors.lightGray,
            ),
            activeIcon: SvgPicture.asset(
              'assets/icons/vote.svg',
              color: AppColors.primary,
            ),
            label: 'Голосування',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/icons/chart.svg',
              color: AppColors.lightGray,
            ),
            activeIcon: SvgPicture.asset(
              'assets/icons/chart.svg',
              color: AppColors.primary,
            ),
            label: 'Статистика',
          ),
        ],
        onTap: (index) => setState(() {
          pageIndex = index;
          title = pageIndex == 0 ? 'Голосування' : 'Статистика';
          elevation = pageIndex == 0 ? 0 : null;
          controller.jumpToPage(pageIndex);
        }),
      ),
    );
  }
}
