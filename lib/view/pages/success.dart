import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/view/pages/home/home.dart';
import 'package:holosuvalka/view/widgets/buttons/button.dart';

class SuccessPage extends StatefulWidget {
  const SuccessPage({Key? key}) : super(key: key);

  @override
  _SuccessPageState createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  String? ip;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(0, 64.0, 0, 8.0),
              child: SvgPicture.asset('assets/illustrations/checkbox.svg'),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 32.0),
              child: Text(
                'Ваш голос '
                'зараховано!',
                textAlign: TextAlign.center,
                style: AppTextStyles.bold32(AppColors.darkGray),
              ),
            ),
            InkWell(
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomePage(),
                ),
              ),
              child: Button(label: 'Далі', filled: true),
            ),
            InkWell(
              onTap: () => Navigator.pop(context),
              child: Button(label: 'Змінити голос', filled: false),
            ),
          ],
        ),
      ),
    );
  }
}
