import 'package:flutter/material.dart';
import 'package:holosuvalka/controller/api.dart';
import 'package:holosuvalka/view/pages/auth/login.dart';

class IpPage extends StatefulWidget {
  const IpPage({Key? key}) : super(key: key);

  @override
  _IpPageState createState() => _IpPageState();
}

class _IpPageState extends State<IpPage> {
  String ip = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextField(
          onChanged: (text) => setState(() => ip = text),
        ),
      ),
      floatingActionButton: ip.isNotEmpty
          ? FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    Api.ip = ip;
                    return LoginPage();
                  },
                ),
              ),
            )
          : null,
    );
  }
}
