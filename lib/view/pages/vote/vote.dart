import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:holosuvalka/controller/api.dart';
import 'package:holosuvalka/view/widgets/buttons/button.dart';
import 'package:intl/intl.dart';
import 'package:holosuvalka/model/election/election.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/view/widgets/question.dart';
import 'package:holosuvalka/view/widgets/white_app_bar.dart';
import 'package:holosuvalka/view/ext/election_type_ext.dart';
import 'package:holosuvalka/controller/bloc/vote_cubit.dart';

class VotePage extends StatelessWidget {
  final Election election;

  const VotePage(this.election) : super();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VoteCubit>(
      create: (_) => VoteCubit(election.questionnaire.asVote),
      child: Scaffold(
        backgroundColor: election.type.color,
        appBar: WhiteAppBar(election.questionnaire.name),
        body: SingleChildScrollView(
          child: BlocBuilder<VoteCubit, VoteState>(
            builder: (context, state) => Column(
              children: [
                SizedBox(
                  height: 64,
                  child: Center(
                    child: Text(
                      'Доступно до '
                      '${DateFormat('yyyy-MM-dd').format(election.endDatetime)}',
                      style: AppTextStyles.semiBold14(AppColors.darkGray),
                      softWrap: true,
                    ),
                  ),
                ),
                ..._buildQuestions(),
                Button(
                  label: 'Підтвердити',
                  filled: true,
                  onClick: () async {
                    final response = await Api.instance.sendVote(state.vote);
                    response.map(
                      success: (response) {
                        print(response);
                        Navigator.pop(context);
                      },
                      fail: print,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildQuestions() => election.questionnaire.questions
      .map((question) => QuestionTile(question))
      .toList();
}
