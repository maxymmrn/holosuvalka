import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:holosuvalka/controller/api.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/view/pages/auth/registration.dart';
import 'package:holosuvalka/view/pages/home/home.dart';
import 'package:holosuvalka/view/widgets/buttons/button.dart';
import 'package:holosuvalka/view/widgets/text_input_fields/custom_input_field.dart';
import 'package:holosuvalka/model/user/user.dart';

class LoginPage extends StatefulWidget {
  const LoginPage() : super();

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? email;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 64, bottom: 32),
                child: SvgPicture.asset('assets/illustrations/start.svg'),
              ),
              Text(
                'Голосувалка!',
                textAlign: TextAlign.center,
                style: AppTextStyles.bold32(AppColors.primary),
              ),
              CustomInputField(
                label: 'Електронна пошта',
                hint: 'example@gmail.com',
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) => email = value,
              ),
              CustomInputField(
                label: 'Пароль',
                icon: const Icon(Icons.remove_red_eye_rounded),
                onChanged: (value) => password = value,
              ),
              Button(
                label: 'Увійти',
                filled: true,
                onClick: () async {
                  if (email != null && password != null) {
                    final response = await Api.instance.login(
                      email!,
                      password!,
                    );

                    response.map(
                      success: (userResponse) {
                        final user = User.authorized(userResponse.data);
                        Api.user = user as AuthorizedUser;
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (_) => HomePage()),
                        );
                      },
                      fail: (failure) => print('FAILED LOGIN: $failure'),
                    );
                  }
                },
              ),
              Button(
                label: 'Зареєструватися',
                filled: false,
                onClick: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) => RegisterPage()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
