import 'package:flutter/material.dart';
import 'package:holosuvalka/controller/api.dart';
import 'package:holosuvalka/style/color.dart';
import 'package:holosuvalka/style/font.dart';
import 'package:holosuvalka/view/pages/home/home.dart';
import 'package:holosuvalka/view/widgets/buttons/button.dart';
import 'package:holosuvalka/view/widgets/text_input_fields/custom_input_field.dart';
import 'package:holosuvalka/view/widgets/white_app_bar.dart';
import 'package:holosuvalka/model/user/user.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage() : super();

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  static const cities = [
    const Location('Lvivska', 'Lviv'),
    const Location('Kyivska', 'Kyiv'),
    const Location('Odesa', 'Odesa'),
  ];

  UnauthorizedUser user = const UnauthorizedUser();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar('Реєстрація'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CustomInputField(
              label: 'Прівище Ім`я',
              hint: '',
              onChanged: (value) => setState(() {
                final data = value.split(' ');
                user = user.copyWith(firstName: data[0], lastName: data[1]);
              }),
            ),
            CustomInputField(
              label: 'Пароль',
              hint: '',
              keyboardType: TextInputType.visiblePassword,
              onChanged: (value) => setState(() {
                user = user.copyWith(password: value);
              }),
            ),
            CustomInputField(
              label: 'День народження',
              hint: 'yyyy-mm-dd',
              keyboardType: TextInputType.datetime,
              onChanged: (value) => setState(() {
                user = user.copyWith(birthDate: DateTime.parse(value));
              }),
            ),
            CustomInputField(
              label: 'Номер паспорта (XXXXXXXXX)',
              hint: 'XXXXXXXXX',
              keyboardType: TextInputType.number,
              onChanged: (value) => setState(() {
                user = user.copyWith(passportNumber: int.tryParse(value));
              }),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 0, 16),
              child: _buildDropdown(context),
            ),
            CustomInputField(
              label: 'ІПН (ХХХХХXXXXX)',
              hint: 'ХХХХХXXXXX',
              keyboardType: TextInputType.number,
              onChanged: (value) => setState(() {
                user = user.copyWith(identificationNumber: int.tryParse(value));
              }),
            ),
            CustomInputField(
              label: 'Електронна пошта',
              hint: 'example@gmail.com',
              keyboardType: TextInputType.emailAddress,
              onChanged: (value) => setState(() {
                user = user.copyWith(email: value);
              }),
            ),
          ],
        ),
      ),
      bottomNavigationBar: user.readyForRegistration
          ? Padding(
              padding: const EdgeInsets.only(bottom: 6),
              child: Button(
                label: 'Зареєструватися',
                filled: true,
                onClick: () async {
                  final response = await Api.instance.register(user);
                  response.map(
                    success: (userResponse) {
                      final user = User.authorized(userResponse.data);
                      Api.user = user as AuthorizedUser;
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => HomePage()),
                      );
                    },
                    fail: (failure) => print('FAILED REGISTRATION: $failure'),
                  );
                },
              ),
            )
          : null,
    );
  }

  Widget _buildDropdown(BuildContext context) {
    return DropdownButtonFormField<Location>(
      value: user.location,
      itemHeight: 48,
      elevation: 16,
      iconSize: 24,
      icon: const Icon(Icons.arrow_drop_down),
      decoration: const InputDecoration(labelText: 'Місце прописки'),
      onChanged: (location) => user = user.copyWith(location: location),
      items: cities
          .map((value) => DropdownMenuItem<Location>(
              value: value,
              child: Container(
                height: 48,
                width: MediaQuery.of(context).size.width - 56,
                alignment: Alignment.centerLeft,
                child: Text(
                  value.city,
                  style: AppTextStyles.regular16(AppColors.darkGray),
                ),
              )))
          .toList(),
    );
  }
}
