// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'vote_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$VoteStateTearOff {
  const _$VoteStateTearOff();

  _VoteState call(Map<int, int> vote, bool flag) {
    return _VoteState(
      vote,
      flag,
    );
  }
}

/// @nodoc
const $VoteState = _$VoteStateTearOff();

/// @nodoc
mixin _$VoteState {
  Map<int, int> get vote => throw _privateConstructorUsedError;
  bool get flag => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $VoteStateCopyWith<VoteState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoteStateCopyWith<$Res> {
  factory $VoteStateCopyWith(VoteState value, $Res Function(VoteState) then) =
      _$VoteStateCopyWithImpl<$Res>;
  $Res call({Map<int, int> vote, bool flag});
}

/// @nodoc
class _$VoteStateCopyWithImpl<$Res> implements $VoteStateCopyWith<$Res> {
  _$VoteStateCopyWithImpl(this._value, this._then);

  final VoteState _value;
  // ignore: unused_field
  final $Res Function(VoteState) _then;

  @override
  $Res call({
    Object? vote = freezed,
    Object? flag = freezed,
  }) {
    return _then(_value.copyWith(
      vote: vote == freezed
          ? _value.vote
          : vote // ignore: cast_nullable_to_non_nullable
              as Map<int, int>,
      flag: flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$VoteStateCopyWith<$Res> implements $VoteStateCopyWith<$Res> {
  factory _$VoteStateCopyWith(
          _VoteState value, $Res Function(_VoteState) then) =
      __$VoteStateCopyWithImpl<$Res>;
  @override
  $Res call({Map<int, int> vote, bool flag});
}

/// @nodoc
class __$VoteStateCopyWithImpl<$Res> extends _$VoteStateCopyWithImpl<$Res>
    implements _$VoteStateCopyWith<$Res> {
  __$VoteStateCopyWithImpl(_VoteState _value, $Res Function(_VoteState) _then)
      : super(_value, (v) => _then(v as _VoteState));

  @override
  _VoteState get _value => super._value as _VoteState;

  @override
  $Res call({
    Object? vote = freezed,
    Object? flag = freezed,
  }) {
    return _then(_VoteState(
      vote == freezed
          ? _value.vote
          : vote // ignore: cast_nullable_to_non_nullable
              as Map<int, int>,
      flag == freezed
          ? _value.flag
          : flag // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_VoteState implements _VoteState {
  const _$_VoteState(this.vote, this.flag);

  @override
  final Map<int, int> vote;
  @override
  final bool flag;

  @override
  String toString() {
    return 'VoteState(vote: $vote, flag: $flag)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _VoteState &&
            (identical(other.vote, vote) ||
                const DeepCollectionEquality().equals(other.vote, vote)) &&
            (identical(other.flag, flag) ||
                const DeepCollectionEquality().equals(other.flag, flag)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(vote) ^
      const DeepCollectionEquality().hash(flag);

  @JsonKey(ignore: true)
  @override
  _$VoteStateCopyWith<_VoteState> get copyWith =>
      __$VoteStateCopyWithImpl<_VoteState>(this, _$identity);
}

abstract class _VoteState implements VoteState {
  const factory _VoteState(Map<int, int> vote, bool flag) = _$_VoteState;

  @override
  Map<int, int> get vote => throw _privateConstructorUsedError;
  @override
  bool get flag => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$VoteStateCopyWith<_VoteState> get copyWith =>
      throw _privateConstructorUsedError;
}
