import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'vote_state.dart';

part 'vote_cubit.freezed.dart';

class VoteCubit extends Cubit<VoteState> {
  VoteCubit(Map<int, int> vote) : super(VoteState(vote, false));

  void switchRadioValue(int key, List<int> sharedRadios) {
    sharedRadios.forEach((radioKey) {
      state.vote[radioKey] = 0;
    });
    state.vote[key] = 1;
    emit(state.copyWith(flag: !state.flag));
  }

  void switchCheckboxValue(int key) {
    bool wasOn = state.vote[key]! > 0;
    state.vote[key] = wasOn ? 0 : 1;
    emit(state.copyWith(flag: !state.flag));
  }

  void setDistributionPoints(int key, int points) {
    state.vote[key] = points;
    emit(state.copyWith(flag: !state.flag));
  }

  int? getValue(int key) => state.vote[key];
}
