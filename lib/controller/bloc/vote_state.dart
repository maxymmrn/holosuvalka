part of 'vote_cubit.dart';

@freezed
class VoteState with _$VoteState {
  const factory VoteState(Map<int, int> vote, bool flag) = _VoteState;
}