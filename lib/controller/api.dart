import 'dart:convert';

import 'package:holosuvalka/controller/repository/auth.dart';
import 'package:holosuvalka/controller/repository/statistics.dart';
import 'package:holosuvalka/controller/repository/vote.dart';
import 'package:holosuvalka/model/election/election.dart';
import 'package:http/http.dart';

import 'package:holosuvalka/model/user/user.dart';

import 'http_result.dart';

class Api with AuthRepository, VoteRepository, StatisticsRepository {
  late String _route;
  late AuthorizedUser _user;
  static Api? _instance;

  Api._();

  static Api get instance => _instance ??= Api._();

  static set ip(String ip) {
    final regex = RegExp('((25[0-5]|2[0-4][0-9]|[1]?[0-9][0-9]?)(\.)?){4}');
    assert(regex.hasMatch(ip), 'IP is invalid $ip');

    instance._route = ip;
  }

  static set user(AuthorizedUser user) => instance._user = user;

  @override
  Future<HttpResult<List<Election>>> getElections() async {
    try {
      final response = await get(
        Uri.parse('http://$_route:5000/election'),
        headers: {
          'Authorization': _user.token.replaceAll('"', ''),
        },
      );
      if (response.statusCode == 200) {
        final decResp = jsonDecode(response.body) as List;
        return HttpResult.success(
          decResp.map((q) => Election.fromJson(q)).toList(),
        );
      } else {
        return HttpResult.fail(response.statusCode, response.reasonPhrase);
      }
    } catch (e) {
      return HttpResult.fail(-1, e.toString());
    }
  }

  @override
  Future<HttpResult> sendVote(Map<int, int> vote) async {
    try {
      final response = await post(
        Uri.parse('http://$_route:5000/vote/'),
        headers: {
          'Authorization': _user.token.replaceAll('"', ''),
        },
        body: jsonEncode({
          'answers': vote.map((key, value) => MapEntry(key.toString(), value)),
        }),
      );
      if (response.statusCode >= 200 && response.statusCode < 300) {
        return HttpResult.success(response.body);
      } else {
        return HttpResult.fail(response.statusCode, response.reasonPhrase);
      }
    } catch (e) {
      return HttpResult.fail(-1, e.toString());
    }
  }

  @override
  Future<HttpResult<String>> login(String email, String password) async {
    try {
      final response = await post(
        Uri.parse('http://$_route:5000/auth/login'),
        body: jsonEncode({'email': email, 'password': password}),
      );
      if (response.statusCode == 200) {
        final token = response.body;
        return HttpResult.success(token);
      } else {
        return HttpResult.fail(response.statusCode, response.reasonPhrase);
      }
    } catch (e) {
      return HttpResult.fail(-1, e.toString());
    }
  }

  @override
  Future<HttpResult<String>> register(User user) async {
    try {
      final url = Uri.parse('http://$_route:5000/auth/register');
      print(jsonEncode(user.toJson()));
      final response = await post(url, body: jsonEncode(user.toJson()));
      print(response.body);
      if (response.statusCode >= 200 && response.statusCode < 300) {
        final token = response.body;
        return HttpResult.success(token);
      } else {
        return HttpResult.fail(response.statusCode, response.reasonPhrase);
      }
    } catch (e) {
      return HttpResult.fail(-1, e.toString());
    }
  }
}
