// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'http_result.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$HttpResultTearOff {
  const _$HttpResultTearOff();

  _HttpSuccess<T> success<T>(T data) {
    return _HttpSuccess<T>(
      data,
    );
  }

  _HttpFail<T> fail<T>(int status, String? message) {
    return _HttpFail<T>(
      status,
      message,
    );
  }
}

/// @nodoc
const $HttpResult = _$HttpResultTearOff();

/// @nodoc
mixin _$HttpResult<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T data) success,
    required TResult Function(int status, String? message) fail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T data)? success,
    TResult Function(int status, String? message)? fail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_HttpSuccess<T> value) success,
    required TResult Function(_HttpFail<T> value) fail,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_HttpSuccess<T> value)? success,
    TResult Function(_HttpFail<T> value)? fail,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HttpResultCopyWith<T, $Res> {
  factory $HttpResultCopyWith(
          HttpResult<T> value, $Res Function(HttpResult<T>) then) =
      _$HttpResultCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$HttpResultCopyWithImpl<T, $Res>
    implements $HttpResultCopyWith<T, $Res> {
  _$HttpResultCopyWithImpl(this._value, this._then);

  final HttpResult<T> _value;
  // ignore: unused_field
  final $Res Function(HttpResult<T>) _then;
}

/// @nodoc
abstract class _$HttpSuccessCopyWith<T, $Res> {
  factory _$HttpSuccessCopyWith(
          _HttpSuccess<T> value, $Res Function(_HttpSuccess<T>) then) =
      __$HttpSuccessCopyWithImpl<T, $Res>;
  $Res call({T data});
}

/// @nodoc
class __$HttpSuccessCopyWithImpl<T, $Res>
    extends _$HttpResultCopyWithImpl<T, $Res>
    implements _$HttpSuccessCopyWith<T, $Res> {
  __$HttpSuccessCopyWithImpl(
      _HttpSuccess<T> _value, $Res Function(_HttpSuccess<T>) _then)
      : super(_value, (v) => _then(v as _HttpSuccess<T>));

  @override
  _HttpSuccess<T> get _value => super._value as _HttpSuccess<T>;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_HttpSuccess<T>(
      data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$_HttpSuccess<T> implements _HttpSuccess<T> {
  const _$_HttpSuccess(this.data);

  @override
  final T data;

  @override
  String toString() {
    return 'HttpResult<$T>.success(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _HttpSuccess<T> &&
            (identical(other.data, data) ||
                const DeepCollectionEquality().equals(other.data, data)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(data);

  @JsonKey(ignore: true)
  @override
  _$HttpSuccessCopyWith<T, _HttpSuccess<T>> get copyWith =>
      __$HttpSuccessCopyWithImpl<T, _HttpSuccess<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T data) success,
    required TResult Function(int status, String? message) fail,
  }) {
    return success(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T data)? success,
    TResult Function(int status, String? message)? fail,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_HttpSuccess<T> value) success,
    required TResult Function(_HttpFail<T> value) fail,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_HttpSuccess<T> value)? success,
    TResult Function(_HttpFail<T> value)? fail,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class _HttpSuccess<T> implements HttpResult<T> {
  const factory _HttpSuccess(T data) = _$_HttpSuccess<T>;

  T get data => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$HttpSuccessCopyWith<T, _HttpSuccess<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$HttpFailCopyWith<T, $Res> {
  factory _$HttpFailCopyWith(
          _HttpFail<T> value, $Res Function(_HttpFail<T>) then) =
      __$HttpFailCopyWithImpl<T, $Res>;
  $Res call({int status, String? message});
}

/// @nodoc
class __$HttpFailCopyWithImpl<T, $Res> extends _$HttpResultCopyWithImpl<T, $Res>
    implements _$HttpFailCopyWith<T, $Res> {
  __$HttpFailCopyWithImpl(
      _HttpFail<T> _value, $Res Function(_HttpFail<T>) _then)
      : super(_value, (v) => _then(v as _HttpFail<T>));

  @override
  _HttpFail<T> get _value => super._value as _HttpFail<T>;

  @override
  $Res call({
    Object? status = freezed,
    Object? message = freezed,
  }) {
    return _then(_HttpFail<T>(
      status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as int,
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_HttpFail<T> implements _HttpFail<T> {
  const _$_HttpFail(this.status, this.message);

  @override
  final int status;
  @override
  final String? message;

  @override
  String toString() {
    return 'HttpResult<$T>.fail(status: $status, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _HttpFail<T> &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)) &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(status) ^
      const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$HttpFailCopyWith<T, _HttpFail<T>> get copyWith =>
      __$HttpFailCopyWithImpl<T, _HttpFail<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T data) success,
    required TResult Function(int status, String? message) fail,
  }) {
    return fail(status, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T data)? success,
    TResult Function(int status, String? message)? fail,
    required TResult orElse(),
  }) {
    if (fail != null) {
      return fail(status, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_HttpSuccess<T> value) success,
    required TResult Function(_HttpFail<T> value) fail,
  }) {
    return fail(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_HttpSuccess<T> value)? success,
    TResult Function(_HttpFail<T> value)? fail,
    required TResult orElse(),
  }) {
    if (fail != null) {
      return fail(this);
    }
    return orElse();
  }
}

abstract class _HttpFail<T> implements HttpResult<T> {
  const factory _HttpFail(int status, String? message) = _$_HttpFail<T>;

  int get status => throw _privateConstructorUsedError;
  String? get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$HttpFailCopyWith<T, _HttpFail<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
