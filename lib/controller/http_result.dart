import 'package:freezed_annotation/freezed_annotation.dart';

part 'http_result.freezed.dart';

@freezed
class HttpResult<T> with _$HttpResult {
  const factory HttpResult.success(T data) = _HttpSuccess<T>;

  const factory HttpResult.fail(int status, String? message) = _HttpFail<T>;
}
