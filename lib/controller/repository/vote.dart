import 'package:holosuvalka/model/election/election.dart';

import '../http_result.dart';

abstract class VoteRepository {
  Future<HttpResult<List<Election>>> getElections();

  Future<HttpResult> sendVote(Map<int, int> vote);
}
