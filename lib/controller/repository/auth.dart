import 'package:holosuvalka/controller/http_result.dart';
import 'package:holosuvalka/model/user/user.dart';

abstract class AuthRepository {
  Future<HttpResult<String>> login(String email, String password);

  Future<HttpResult<String>> register(User user);
}
