import 'dart:ui';

import 'package:flutter/material.dart';
abstract class AppColors {
  static const primary = MaterialColor(0xFF3529FF, {900: Color(0xFF3529FF)});

  static const darkGray = Color(0xFF232230);
  static const mediumGray = Color(0xFF79769E);
  static const lightGray = Color(0xFFA7A5C0);

  static const background = Color(0xFFFFFFFF);
  static const nationalBackground = Color(0xFFF5F5FF);
  static const localBackground = Color(0xFFFFF0F0);
  static const organizationBackground = Color(0xFFFFF4D6);

  static const chartDarkBlue = Color(0xFFCCCCFF);
  static const chartOrange = Color(0xFFFFEFC2);
  static const chartRed = Color(0xFFFFCCCC);
  static const chartPink = Color(0xFFF1B8FF);
  static const charGreen = Color(0xFFC2FFB8);
  static const chartLightBlue = Color(0xFFC2FFDE);
  static const chartGray = Color(0xFFC4C3D5);
}