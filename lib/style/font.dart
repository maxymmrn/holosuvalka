import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'color.dart';

abstract class AppTextStyles {
  static TextStyle bold32(Color color) {
    return TextStyle(
      fontSize: 32,
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.bold,
      color: color,
    );
  }

  static TextStyle bold12(Color color) {
    return TextStyle(
      fontSize: 12,
      fontFamily: 'OpenSans-Bold',
      height: 2,
      fontWeight: FontWeight.bold,
      color: color,
    );
  }

  static TextStyle semiBold18(Color color) {
    return TextStyle(
      fontSize: 18,
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle semiBold14(Color color) {
    return TextStyle(
      fontSize: 14,
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle semiBold12(Color color) {
    return TextStyle(
      fontSize: 12,
      height: 2,
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle semiBold10(Color color) {
    return TextStyle(
      fontSize: 10,
      fontFamily: 'OpenSans',
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle regular20(Color color) {
    return TextStyle(
      fontSize: 20,
      height: 1.2,
      fontFamily: 'OpenSans',
      color: color,
    );
  }

  static TextStyle regular16(Color color) {
    return TextStyle(
        fontSize: 16,
        fontFamily: 'OpenSans',
        letterSpacing: 0.15,
        height: 1.5,
        color: color);
  }

  static TextStyle regular12(Color color) {
    return TextStyle(
        fontSize: 12,
        fontFamily: 'OpenSans',
        letterSpacing: 0.4,
        height: 1.4,
        color: color);
  }

  static TextStyle regular10(Color color) {
    return TextStyle(
        fontSize: 10,
        fontFamily: 'OpenSans',
        color: color);
  }

  static TextStyle regular8(Color color) {
    return TextStyle(
        fontSize: 8,
        fontFamily: 'OpenSans',
        color: color);
  }
}
