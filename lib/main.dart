import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:holosuvalka/view/pages/ip.dart';
import 'package:holosuvalka/style/color.dart';

void main() => runApp(MaterialApp(
      title: 'Holosuvalka',
      theme: ThemeData(
        primaryColor: AppColors.primary,
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const IpPage(),
    ));
