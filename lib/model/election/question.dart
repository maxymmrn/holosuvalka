part of 'election.dart';

@freezed
class Question with _$Question {
  const factory Question(
    @JsonKey(name: 'text') String description,
    @JsonKey(name: 'question_type') QuestionType questionType,
    @JsonKey(name: 'points', defaultValue: 0) int points,
    @JsonKey(name: 'choices') List<Choice> choices,
  ) = _Question;

  factory Question.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$QuestionFromJson(json);
}

enum QuestionType {
  @JsonValue('radio_item')
  radioItem,
  @JsonValue('checkbox')
  checkbox,
  @JsonValue('distribution')
  distribution,
}
