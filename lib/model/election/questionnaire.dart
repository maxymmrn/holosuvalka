part of 'election.dart';

@freezed
class Questionnaire with _$Questionnaire {
  const Questionnaire._();

  const factory Questionnaire(
    @JsonKey(name: 'name') String name,
    @JsonKey(name: 'questions') List<Question> questions,
  ) = _Questionnaire;

  factory Questionnaire.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$QuestionnaireFromJson(json);

  Map<int, int> get asVote => Map<int, int>.fromEntries(
        questions
            .expand((question) => question.choices)
            .map((choice) => MapEntry(choice.id, choice.points)),
      );
}
