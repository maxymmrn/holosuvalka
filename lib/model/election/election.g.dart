// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'election.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Election _$_$_ElectionFromJson(Map<String, dynamic> json) {
  return _$_Election(
    id: json['id'] as int,
    type: _$enumDecode(_$ElectionTypeEnumMap, json['type']),
    questionnaire:
        Questionnaire.fromJson(json['questionnaire'] as Map<String, dynamic>),
    endDatetime: DateTime.parse(json['end_datetime'] as String),
  );
}

Map<String, dynamic> _$_$_ElectionToJson(_$_Election instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$ElectionTypeEnumMap[instance.type],
      'questionnaire': instance.questionnaire,
      'end_datetime': instance.endDatetime.toIso8601String(),
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$ElectionTypeEnumMap = {
  ElectionType.national: 'national',
  ElectionType.local: 'local',
  ElectionType.organisation: 'organisation',
};

_$_Questionnaire _$_$_QuestionnaireFromJson(Map<String, dynamic> json) {
  return _$_Questionnaire(
    json['name'] as String,
    (json['questions'] as List<dynamic>)
        .map((e) => Question.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$_QuestionnaireToJson(_$_Questionnaire instance) =>
    <String, dynamic>{
      'name': instance.name,
      'questions': instance.questions,
    };

_$_Question _$_$_QuestionFromJson(Map<String, dynamic> json) {
  return _$_Question(
    json['text'] as String,
    _$enumDecode(_$QuestionTypeEnumMap, json['question_type']),
    json['points'] as int? ?? 0,
    (json['choices'] as List<dynamic>)
        .map((e) => Choice.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$_$_QuestionToJson(_$_Question instance) =>
    <String, dynamic>{
      'text': instance.description,
      'question_type': _$QuestionTypeEnumMap[instance.questionType],
      'points': instance.points,
      'choices': instance.choices,
    };

const _$QuestionTypeEnumMap = {
  QuestionType.radioItem: 'radio_item',
  QuestionType.checkbox: 'checkbox',
  QuestionType.distribution: 'distribution',
};

_$_Choice _$_$_ChoiceFromJson(Map<String, dynamic> json) {
  return _$_Choice(
    id: json['id'] as int,
    answer: json['text'] as String,
    points: json['points'] as int? ?? 0,
  );
}

Map<String, dynamic> _$_$_ChoiceToJson(_$_Choice instance) => <String, dynamic>{
      'id': instance.id,
      'text': instance.answer,
      'points': instance.points,
    };
