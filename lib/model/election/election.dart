import 'package:freezed_annotation/freezed_annotation.dart';

part 'questionnaire.dart';

part 'question.dart';

part 'choice.dart';

part 'election.freezed.dart';

part 'election.g.dart';

@freezed
class Election with _$Election {
  const factory Election({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'type') required ElectionType type,
    @JsonKey(name: 'questionnaire') required Questionnaire questionnaire,
    @JsonKey(name: 'end_datetime') required DateTime endDatetime,
  }) = _Election;

  factory Election.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$ElectionFromJson(json);
}

enum ElectionType {
  national,
  local,
  organisation,
}
