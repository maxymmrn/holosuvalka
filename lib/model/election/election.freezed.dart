// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'election.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Election _$ElectionFromJson(Map<String, dynamic> json) {
  return _Election.fromJson(json);
}

/// @nodoc
class _$ElectionTearOff {
  const _$ElectionTearOff();

  _Election call(
      {@JsonKey(name: 'id') required int id,
      @JsonKey(name: 'type') required ElectionType type,
      @JsonKey(name: 'questionnaire') required Questionnaire questionnaire,
      @JsonKey(name: 'end_datetime') required DateTime endDatetime}) {
    return _Election(
      id: id,
      type: type,
      questionnaire: questionnaire,
      endDatetime: endDatetime,
    );
  }

  Election fromJson(Map<String, Object> json) {
    return Election.fromJson(json);
  }
}

/// @nodoc
const $Election = _$ElectionTearOff();

/// @nodoc
mixin _$Election {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'type')
  ElectionType get type => throw _privateConstructorUsedError;
  @JsonKey(name: 'questionnaire')
  Questionnaire get questionnaire => throw _privateConstructorUsedError;
  @JsonKey(name: 'end_datetime')
  DateTime get endDatetime => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ElectionCopyWith<Election> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ElectionCopyWith<$Res> {
  factory $ElectionCopyWith(Election value, $Res Function(Election) then) =
      _$ElectionCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'type') ElectionType type,
      @JsonKey(name: 'questionnaire') Questionnaire questionnaire,
      @JsonKey(name: 'end_datetime') DateTime endDatetime});

  $QuestionnaireCopyWith<$Res> get questionnaire;
}

/// @nodoc
class _$ElectionCopyWithImpl<$Res> implements $ElectionCopyWith<$Res> {
  _$ElectionCopyWithImpl(this._value, this._then);

  final Election _value;
  // ignore: unused_field
  final $Res Function(Election) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? type = freezed,
    Object? questionnaire = freezed,
    Object? endDatetime = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as ElectionType,
      questionnaire: questionnaire == freezed
          ? _value.questionnaire
          : questionnaire // ignore: cast_nullable_to_non_nullable
              as Questionnaire,
      endDatetime: endDatetime == freezed
          ? _value.endDatetime
          : endDatetime // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }

  @override
  $QuestionnaireCopyWith<$Res> get questionnaire {
    return $QuestionnaireCopyWith<$Res>(_value.questionnaire, (value) {
      return _then(_value.copyWith(questionnaire: value));
    });
  }
}

/// @nodoc
abstract class _$ElectionCopyWith<$Res> implements $ElectionCopyWith<$Res> {
  factory _$ElectionCopyWith(_Election value, $Res Function(_Election) then) =
      __$ElectionCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'type') ElectionType type,
      @JsonKey(name: 'questionnaire') Questionnaire questionnaire,
      @JsonKey(name: 'end_datetime') DateTime endDatetime});

  @override
  $QuestionnaireCopyWith<$Res> get questionnaire;
}

/// @nodoc
class __$ElectionCopyWithImpl<$Res> extends _$ElectionCopyWithImpl<$Res>
    implements _$ElectionCopyWith<$Res> {
  __$ElectionCopyWithImpl(_Election _value, $Res Function(_Election) _then)
      : super(_value, (v) => _then(v as _Election));

  @override
  _Election get _value => super._value as _Election;

  @override
  $Res call({
    Object? id = freezed,
    Object? type = freezed,
    Object? questionnaire = freezed,
    Object? endDatetime = freezed,
  }) {
    return _then(_Election(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as ElectionType,
      questionnaire: questionnaire == freezed
          ? _value.questionnaire
          : questionnaire // ignore: cast_nullable_to_non_nullable
              as Questionnaire,
      endDatetime: endDatetime == freezed
          ? _value.endDatetime
          : endDatetime // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Election implements _Election {
  const _$_Election(
      {@JsonKey(name: 'id') required this.id,
      @JsonKey(name: 'type') required this.type,
      @JsonKey(name: 'questionnaire') required this.questionnaire,
      @JsonKey(name: 'end_datetime') required this.endDatetime});

  factory _$_Election.fromJson(Map<String, dynamic> json) =>
      _$_$_ElectionFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'type')
  final ElectionType type;
  @override
  @JsonKey(name: 'questionnaire')
  final Questionnaire questionnaire;
  @override
  @JsonKey(name: 'end_datetime')
  final DateTime endDatetime;

  @override
  String toString() {
    return 'Election(id: $id, type: $type, questionnaire: $questionnaire, endDatetime: $endDatetime)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Election &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.questionnaire, questionnaire) ||
                const DeepCollectionEquality()
                    .equals(other.questionnaire, questionnaire)) &&
            (identical(other.endDatetime, endDatetime) ||
                const DeepCollectionEquality()
                    .equals(other.endDatetime, endDatetime)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(questionnaire) ^
      const DeepCollectionEquality().hash(endDatetime);

  @JsonKey(ignore: true)
  @override
  _$ElectionCopyWith<_Election> get copyWith =>
      __$ElectionCopyWithImpl<_Election>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ElectionToJson(this);
  }
}

abstract class _Election implements Election {
  const factory _Election(
          {@JsonKey(name: 'id') required int id,
          @JsonKey(name: 'type') required ElectionType type,
          @JsonKey(name: 'questionnaire') required Questionnaire questionnaire,
          @JsonKey(name: 'end_datetime') required DateTime endDatetime}) =
      _$_Election;

  factory _Election.fromJson(Map<String, dynamic> json) = _$_Election.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'type')
  ElectionType get type => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'questionnaire')
  Questionnaire get questionnaire => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'end_datetime')
  DateTime get endDatetime => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ElectionCopyWith<_Election> get copyWith =>
      throw _privateConstructorUsedError;
}

Questionnaire _$QuestionnaireFromJson(Map<String, dynamic> json) {
  return _Questionnaire.fromJson(json);
}

/// @nodoc
class _$QuestionnaireTearOff {
  const _$QuestionnaireTearOff();

  _Questionnaire call(@JsonKey(name: 'name') String name,
      @JsonKey(name: 'questions') List<Question> questions) {
    return _Questionnaire(
      name,
      questions,
    );
  }

  Questionnaire fromJson(Map<String, Object> json) {
    return Questionnaire.fromJson(json);
  }
}

/// @nodoc
const $Questionnaire = _$QuestionnaireTearOff();

/// @nodoc
mixin _$Questionnaire {
  @JsonKey(name: 'name')
  String get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'questions')
  List<Question> get questions => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $QuestionnaireCopyWith<Questionnaire> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuestionnaireCopyWith<$Res> {
  factory $QuestionnaireCopyWith(
          Questionnaire value, $Res Function(Questionnaire) then) =
      _$QuestionnaireCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'name') String name,
      @JsonKey(name: 'questions') List<Question> questions});
}

/// @nodoc
class _$QuestionnaireCopyWithImpl<$Res>
    implements $QuestionnaireCopyWith<$Res> {
  _$QuestionnaireCopyWithImpl(this._value, this._then);

  final Questionnaire _value;
  // ignore: unused_field
  final $Res Function(Questionnaire) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? questions = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      questions: questions == freezed
          ? _value.questions
          : questions // ignore: cast_nullable_to_non_nullable
              as List<Question>,
    ));
  }
}

/// @nodoc
abstract class _$QuestionnaireCopyWith<$Res>
    implements $QuestionnaireCopyWith<$Res> {
  factory _$QuestionnaireCopyWith(
          _Questionnaire value, $Res Function(_Questionnaire) then) =
      __$QuestionnaireCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'name') String name,
      @JsonKey(name: 'questions') List<Question> questions});
}

/// @nodoc
class __$QuestionnaireCopyWithImpl<$Res>
    extends _$QuestionnaireCopyWithImpl<$Res>
    implements _$QuestionnaireCopyWith<$Res> {
  __$QuestionnaireCopyWithImpl(
      _Questionnaire _value, $Res Function(_Questionnaire) _then)
      : super(_value, (v) => _then(v as _Questionnaire));

  @override
  _Questionnaire get _value => super._value as _Questionnaire;

  @override
  $Res call({
    Object? name = freezed,
    Object? questions = freezed,
  }) {
    return _then(_Questionnaire(
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      questions == freezed
          ? _value.questions
          : questions // ignore: cast_nullable_to_non_nullable
              as List<Question>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Questionnaire extends _Questionnaire {
  const _$_Questionnaire(@JsonKey(name: 'name') this.name,
      @JsonKey(name: 'questions') this.questions)
      : super._();

  factory _$_Questionnaire.fromJson(Map<String, dynamic> json) =>
      _$_$_QuestionnaireFromJson(json);

  @override
  @JsonKey(name: 'name')
  final String name;
  @override
  @JsonKey(name: 'questions')
  final List<Question> questions;

  @override
  String toString() {
    return 'Questionnaire(name: $name, questions: $questions)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Questionnaire &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.questions, questions) ||
                const DeepCollectionEquality()
                    .equals(other.questions, questions)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(questions);

  @JsonKey(ignore: true)
  @override
  _$QuestionnaireCopyWith<_Questionnaire> get copyWith =>
      __$QuestionnaireCopyWithImpl<_Questionnaire>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_QuestionnaireToJson(this);
  }
}

abstract class _Questionnaire extends Questionnaire {
  const factory _Questionnaire(@JsonKey(name: 'name') String name,
      @JsonKey(name: 'questions') List<Question> questions) = _$_Questionnaire;
  const _Questionnaire._() : super._();

  factory _Questionnaire.fromJson(Map<String, dynamic> json) =
      _$_Questionnaire.fromJson;

  @override
  @JsonKey(name: 'name')
  String get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'questions')
  List<Question> get questions => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$QuestionnaireCopyWith<_Questionnaire> get copyWith =>
      throw _privateConstructorUsedError;
}

Question _$QuestionFromJson(Map<String, dynamic> json) {
  return _Question.fromJson(json);
}

/// @nodoc
class _$QuestionTearOff {
  const _$QuestionTearOff();

  _Question call(
      @JsonKey(name: 'text') String description,
      @JsonKey(name: 'question_type') QuestionType questionType,
      @JsonKey(name: 'points', defaultValue: 0) int points,
      @JsonKey(name: 'choices') List<Choice> choices) {
    return _Question(
      description,
      questionType,
      points,
      choices,
    );
  }

  Question fromJson(Map<String, Object> json) {
    return Question.fromJson(json);
  }
}

/// @nodoc
const $Question = _$QuestionTearOff();

/// @nodoc
mixin _$Question {
  @JsonKey(name: 'text')
  String get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'question_type')
  QuestionType get questionType => throw _privateConstructorUsedError;
  @JsonKey(name: 'points', defaultValue: 0)
  int get points => throw _privateConstructorUsedError;
  @JsonKey(name: 'choices')
  List<Choice> get choices => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $QuestionCopyWith<Question> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuestionCopyWith<$Res> {
  factory $QuestionCopyWith(Question value, $Res Function(Question) then) =
      _$QuestionCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'text') String description,
      @JsonKey(name: 'question_type') QuestionType questionType,
      @JsonKey(name: 'points', defaultValue: 0) int points,
      @JsonKey(name: 'choices') List<Choice> choices});
}

/// @nodoc
class _$QuestionCopyWithImpl<$Res> implements $QuestionCopyWith<$Res> {
  _$QuestionCopyWithImpl(this._value, this._then);

  final Question _value;
  // ignore: unused_field
  final $Res Function(Question) _then;

  @override
  $Res call({
    Object? description = freezed,
    Object? questionType = freezed,
    Object? points = freezed,
    Object? choices = freezed,
  }) {
    return _then(_value.copyWith(
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      questionType: questionType == freezed
          ? _value.questionType
          : questionType // ignore: cast_nullable_to_non_nullable
              as QuestionType,
      points: points == freezed
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as int,
      choices: choices == freezed
          ? _value.choices
          : choices // ignore: cast_nullable_to_non_nullable
              as List<Choice>,
    ));
  }
}

/// @nodoc
abstract class _$QuestionCopyWith<$Res> implements $QuestionCopyWith<$Res> {
  factory _$QuestionCopyWith(_Question value, $Res Function(_Question) then) =
      __$QuestionCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'text') String description,
      @JsonKey(name: 'question_type') QuestionType questionType,
      @JsonKey(name: 'points', defaultValue: 0) int points,
      @JsonKey(name: 'choices') List<Choice> choices});
}

/// @nodoc
class __$QuestionCopyWithImpl<$Res> extends _$QuestionCopyWithImpl<$Res>
    implements _$QuestionCopyWith<$Res> {
  __$QuestionCopyWithImpl(_Question _value, $Res Function(_Question) _then)
      : super(_value, (v) => _then(v as _Question));

  @override
  _Question get _value => super._value as _Question;

  @override
  $Res call({
    Object? description = freezed,
    Object? questionType = freezed,
    Object? points = freezed,
    Object? choices = freezed,
  }) {
    return _then(_Question(
      description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      questionType == freezed
          ? _value.questionType
          : questionType // ignore: cast_nullable_to_non_nullable
              as QuestionType,
      points == freezed
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as int,
      choices == freezed
          ? _value.choices
          : choices // ignore: cast_nullable_to_non_nullable
              as List<Choice>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Question implements _Question {
  const _$_Question(
      @JsonKey(name: 'text') this.description,
      @JsonKey(name: 'question_type') this.questionType,
      @JsonKey(name: 'points', defaultValue: 0) this.points,
      @JsonKey(name: 'choices') this.choices);

  factory _$_Question.fromJson(Map<String, dynamic> json) =>
      _$_$_QuestionFromJson(json);

  @override
  @JsonKey(name: 'text')
  final String description;
  @override
  @JsonKey(name: 'question_type')
  final QuestionType questionType;
  @override
  @JsonKey(name: 'points', defaultValue: 0)
  final int points;
  @override
  @JsonKey(name: 'choices')
  final List<Choice> choices;

  @override
  String toString() {
    return 'Question(description: $description, questionType: $questionType, points: $points, choices: $choices)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Question &&
            (identical(other.description, description) ||
                const DeepCollectionEquality()
                    .equals(other.description, description)) &&
            (identical(other.questionType, questionType) ||
                const DeepCollectionEquality()
                    .equals(other.questionType, questionType)) &&
            (identical(other.points, points) ||
                const DeepCollectionEquality().equals(other.points, points)) &&
            (identical(other.choices, choices) ||
                const DeepCollectionEquality().equals(other.choices, choices)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(description) ^
      const DeepCollectionEquality().hash(questionType) ^
      const DeepCollectionEquality().hash(points) ^
      const DeepCollectionEquality().hash(choices);

  @JsonKey(ignore: true)
  @override
  _$QuestionCopyWith<_Question> get copyWith =>
      __$QuestionCopyWithImpl<_Question>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_QuestionToJson(this);
  }
}

abstract class _Question implements Question {
  const factory _Question(
      @JsonKey(name: 'text') String description,
      @JsonKey(name: 'question_type') QuestionType questionType,
      @JsonKey(name: 'points', defaultValue: 0) int points,
      @JsonKey(name: 'choices') List<Choice> choices) = _$_Question;

  factory _Question.fromJson(Map<String, dynamic> json) = _$_Question.fromJson;

  @override
  @JsonKey(name: 'text')
  String get description => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'question_type')
  QuestionType get questionType => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'points', defaultValue: 0)
  int get points => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'choices')
  List<Choice> get choices => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$QuestionCopyWith<_Question> get copyWith =>
      throw _privateConstructorUsedError;
}

Choice _$ChoiceFromJson(Map<String, dynamic> json) {
  return _Choice.fromJson(json);
}

/// @nodoc
class _$ChoiceTearOff {
  const _$ChoiceTearOff();

  _Choice call(
      {@JsonKey(name: 'id') required int id,
      @JsonKey(name: 'text') required String answer,
      @JsonKey(name: 'points', defaultValue: 0) required int points}) {
    return _Choice(
      id: id,
      answer: answer,
      points: points,
    );
  }

  Choice fromJson(Map<String, Object> json) {
    return Choice.fromJson(json);
  }
}

/// @nodoc
const $Choice = _$ChoiceTearOff();

/// @nodoc
mixin _$Choice {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'text')
  String get answer => throw _privateConstructorUsedError;
  @JsonKey(name: 'points', defaultValue: 0)
  int get points => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChoiceCopyWith<Choice> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChoiceCopyWith<$Res> {
  factory $ChoiceCopyWith(Choice value, $Res Function(Choice) then) =
      _$ChoiceCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'text') String answer,
      @JsonKey(name: 'points', defaultValue: 0) int points});
}

/// @nodoc
class _$ChoiceCopyWithImpl<$Res> implements $ChoiceCopyWith<$Res> {
  _$ChoiceCopyWithImpl(this._value, this._then);

  final Choice _value;
  // ignore: unused_field
  final $Res Function(Choice) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? answer = freezed,
    Object? points = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as String,
      points: points == freezed
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$ChoiceCopyWith<$Res> implements $ChoiceCopyWith<$Res> {
  factory _$ChoiceCopyWith(_Choice value, $Res Function(_Choice) then) =
      __$ChoiceCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'text') String answer,
      @JsonKey(name: 'points', defaultValue: 0) int points});
}

/// @nodoc
class __$ChoiceCopyWithImpl<$Res> extends _$ChoiceCopyWithImpl<$Res>
    implements _$ChoiceCopyWith<$Res> {
  __$ChoiceCopyWithImpl(_Choice _value, $Res Function(_Choice) _then)
      : super(_value, (v) => _then(v as _Choice));

  @override
  _Choice get _value => super._value as _Choice;

  @override
  $Res call({
    Object? id = freezed,
    Object? answer = freezed,
    Object? points = freezed,
  }) {
    return _then(_Choice(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      answer: answer == freezed
          ? _value.answer
          : answer // ignore: cast_nullable_to_non_nullable
              as String,
      points: points == freezed
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Choice implements _Choice {
  const _$_Choice(
      {@JsonKey(name: 'id') required this.id,
      @JsonKey(name: 'text') required this.answer,
      @JsonKey(name: 'points', defaultValue: 0) required this.points});

  factory _$_Choice.fromJson(Map<String, dynamic> json) =>
      _$_$_ChoiceFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'text')
  final String answer;
  @override
  @JsonKey(name: 'points', defaultValue: 0)
  final int points;

  @override
  String toString() {
    return 'Choice(id: $id, answer: $answer, points: $points)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Choice &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.answer, answer) ||
                const DeepCollectionEquality().equals(other.answer, answer)) &&
            (identical(other.points, points) ||
                const DeepCollectionEquality().equals(other.points, points)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(answer) ^
      const DeepCollectionEquality().hash(points);

  @JsonKey(ignore: true)
  @override
  _$ChoiceCopyWith<_Choice> get copyWith =>
      __$ChoiceCopyWithImpl<_Choice>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ChoiceToJson(this);
  }
}

abstract class _Choice implements Choice {
  const factory _Choice(
          {@JsonKey(name: 'id') required int id,
          @JsonKey(name: 'text') required String answer,
          @JsonKey(name: 'points', defaultValue: 0) required int points}) =
      _$_Choice;

  factory _Choice.fromJson(Map<String, dynamic> json) = _$_Choice.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'text')
  String get answer => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'points', defaultValue: 0)
  int get points => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ChoiceCopyWith<_Choice> get copyWith => throw _privateConstructorUsedError;
}
