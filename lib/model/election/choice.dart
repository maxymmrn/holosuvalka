part of 'election.dart';

@freezed
class Choice with _$Choice {
  const factory Choice({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'text') required String answer,
    @JsonKey(name: 'points', defaultValue: 0) required int points,
  }) = _Choice;

  factory Choice.fromJson(Map<String, dynamic> json) => _$ChoiceFromJson(json);
}
