import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

part 'user.g.dart';

part 'location.dart';

@freezed
class User with _$User {
  const User._();

  const factory User.authorized(String token) = AuthorizedUser;

  const factory User.unauthorized({
    @JsonKey(name: 'email') String? email,
    @JsonKey(name: 'first_name') String? firstName,
    @JsonKey(name: 'last_name') String? lastName,
    @JsonKey(name: 'password') String? password,
    @JsonKey(name: 'identification_number') int? identificationNumber,
    @JsonKey(name: 'passport_number') int? passportNumber,
    @JsonKey(name: 'birth_date') DateTime? birthDate,
    @JsonKey(name: 'location') Location? location,
  }) = UnauthorizedUser;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  bool get readyForRegistration => map(
        authorized: (_) => true,
        unauthorized: (user) =>
            user.email != null &&
            user.firstName != null &&
            user.lastName != null &&
            user.password != null &&
            user.identificationNumber != null &&
            user.passportNumber != null &&
            user.birthDate != null &&
            user.location != null,
      );
}
