// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

User _$UserFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType'] as String) {
    case 'authorized':
      return AuthorizedUser.fromJson(json);
    case 'unauthorized':
      return UnauthorizedUser.fromJson(json);

    default:
      throw FallThroughError();
  }
}

/// @nodoc
class _$UserTearOff {
  const _$UserTearOff();

  AuthorizedUser authorized(String token) {
    return AuthorizedUser(
      token,
    );
  }

  UnauthorizedUser unauthorized(
      {@JsonKey(name: 'email') String? email,
      @JsonKey(name: 'first_name') String? firstName,
      @JsonKey(name: 'last_name') String? lastName,
      @JsonKey(name: 'password') String? password,
      @JsonKey(name: 'identification_number') int? identificationNumber,
      @JsonKey(name: 'passport_number') int? passportNumber,
      @JsonKey(name: 'birth_date') DateTime? birthDate,
      @JsonKey(name: 'location') Location? location}) {
    return UnauthorizedUser(
      email: email,
      firstName: firstName,
      lastName: lastName,
      password: password,
      identificationNumber: identificationNumber,
      passportNumber: passportNumber,
      birthDate: birthDate,
      location: location,
    );
  }

  User fromJson(Map<String, Object> json) {
    return User.fromJson(json);
  }
}

/// @nodoc
const $User = _$UserTearOff();

/// @nodoc
mixin _$User {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String token) authorized,
    required TResult Function(
            @JsonKey(name: 'email') String? email,
            @JsonKey(name: 'first_name') String? firstName,
            @JsonKey(name: 'last_name') String? lastName,
            @JsonKey(name: 'password') String? password,
            @JsonKey(name: 'identification_number') int? identificationNumber,
            @JsonKey(name: 'passport_number') int? passportNumber,
            @JsonKey(name: 'birth_date') DateTime? birthDate,
            @JsonKey(name: 'location') Location? location)
        unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String token)? authorized,
    TResult Function(
            @JsonKey(name: 'email') String? email,
            @JsonKey(name: 'first_name') String? firstName,
            @JsonKey(name: 'last_name') String? lastName,
            @JsonKey(name: 'password') String? password,
            @JsonKey(name: 'identification_number') int? identificationNumber,
            @JsonKey(name: 'passport_number') int? passportNumber,
            @JsonKey(name: 'birth_date') DateTime? birthDate,
            @JsonKey(name: 'location') Location? location)?
        unauthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthorizedUser value) authorized,
    required TResult Function(UnauthorizedUser value) unauthorized,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthorizedUser value)? authorized,
    TResult Function(UnauthorizedUser value)? unauthorized,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserCopyWithImpl<$Res> implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  final User _value;
  // ignore: unused_field
  final $Res Function(User) _then;
}

/// @nodoc
abstract class $AuthorizedUserCopyWith<$Res> {
  factory $AuthorizedUserCopyWith(
          AuthorizedUser value, $Res Function(AuthorizedUser) then) =
      _$AuthorizedUserCopyWithImpl<$Res>;
  $Res call({String token});
}

/// @nodoc
class _$AuthorizedUserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements $AuthorizedUserCopyWith<$Res> {
  _$AuthorizedUserCopyWithImpl(
      AuthorizedUser _value, $Res Function(AuthorizedUser) _then)
      : super(_value, (v) => _then(v as AuthorizedUser));

  @override
  AuthorizedUser get _value => super._value as AuthorizedUser;

  @override
  $Res call({
    Object? token = freezed,
  }) {
    return _then(AuthorizedUser(
      token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AuthorizedUser extends AuthorizedUser {
  const _$AuthorizedUser(this.token) : super._();

  factory _$AuthorizedUser.fromJson(Map<String, dynamic> json) =>
      _$_$AuthorizedUserFromJson(json);

  @override
  final String token;

  @override
  String toString() {
    return 'User.authorized(token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AuthorizedUser &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  $AuthorizedUserCopyWith<AuthorizedUser> get copyWith =>
      _$AuthorizedUserCopyWithImpl<AuthorizedUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String token) authorized,
    required TResult Function(
            @JsonKey(name: 'email') String? email,
            @JsonKey(name: 'first_name') String? firstName,
            @JsonKey(name: 'last_name') String? lastName,
            @JsonKey(name: 'password') String? password,
            @JsonKey(name: 'identification_number') int? identificationNumber,
            @JsonKey(name: 'passport_number') int? passportNumber,
            @JsonKey(name: 'birth_date') DateTime? birthDate,
            @JsonKey(name: 'location') Location? location)
        unauthorized,
  }) {
    return authorized(token);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String token)? authorized,
    TResult Function(
            @JsonKey(name: 'email') String? email,
            @JsonKey(name: 'first_name') String? firstName,
            @JsonKey(name: 'last_name') String? lastName,
            @JsonKey(name: 'password') String? password,
            @JsonKey(name: 'identification_number') int? identificationNumber,
            @JsonKey(name: 'passport_number') int? passportNumber,
            @JsonKey(name: 'birth_date') DateTime? birthDate,
            @JsonKey(name: 'location') Location? location)?
        unauthorized,
    required TResult orElse(),
  }) {
    if (authorized != null) {
      return authorized(token);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthorizedUser value) authorized,
    required TResult Function(UnauthorizedUser value) unauthorized,
  }) {
    return authorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthorizedUser value)? authorized,
    TResult Function(UnauthorizedUser value)? unauthorized,
    required TResult orElse(),
  }) {
    if (authorized != null) {
      return authorized(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$AuthorizedUserToJson(this)..['runtimeType'] = 'authorized';
  }
}

abstract class AuthorizedUser extends User {
  const factory AuthorizedUser(String token) = _$AuthorizedUser;
  const AuthorizedUser._() : super._();

  factory AuthorizedUser.fromJson(Map<String, dynamic> json) =
      _$AuthorizedUser.fromJson;

  String get token => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthorizedUserCopyWith<AuthorizedUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnauthorizedUserCopyWith<$Res> {
  factory $UnauthorizedUserCopyWith(
          UnauthorizedUser value, $Res Function(UnauthorizedUser) then) =
      _$UnauthorizedUserCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'email') String? email,
      @JsonKey(name: 'first_name') String? firstName,
      @JsonKey(name: 'last_name') String? lastName,
      @JsonKey(name: 'password') String? password,
      @JsonKey(name: 'identification_number') int? identificationNumber,
      @JsonKey(name: 'passport_number') int? passportNumber,
      @JsonKey(name: 'birth_date') DateTime? birthDate,
      @JsonKey(name: 'location') Location? location});

  $LocationCopyWith<$Res>? get location;
}

/// @nodoc
class _$UnauthorizedUserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements $UnauthorizedUserCopyWith<$Res> {
  _$UnauthorizedUserCopyWithImpl(
      UnauthorizedUser _value, $Res Function(UnauthorizedUser) _then)
      : super(_value, (v) => _then(v as UnauthorizedUser));

  @override
  UnauthorizedUser get _value => super._value as UnauthorizedUser;

  @override
  $Res call({
    Object? email = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? password = freezed,
    Object? identificationNumber = freezed,
    Object? passportNumber = freezed,
    Object? birthDate = freezed,
    Object? location = freezed,
  }) {
    return _then(UnauthorizedUser(
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      identificationNumber: identificationNumber == freezed
          ? _value.identificationNumber
          : identificationNumber // ignore: cast_nullable_to_non_nullable
              as int?,
      passportNumber: passportNumber == freezed
          ? _value.passportNumber
          : passportNumber // ignore: cast_nullable_to_non_nullable
              as int?,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as Location?,
    ));
  }

  @override
  $LocationCopyWith<$Res>? get location {
    if (_value.location == null) {
      return null;
    }

    return $LocationCopyWith<$Res>(_value.location!, (value) {
      return _then(_value.copyWith(location: value));
    });
  }
}

/// @nodoc
@JsonSerializable()
class _$UnauthorizedUser extends UnauthorizedUser {
  const _$UnauthorizedUser(
      {@JsonKey(name: 'email') this.email,
      @JsonKey(name: 'first_name') this.firstName,
      @JsonKey(name: 'last_name') this.lastName,
      @JsonKey(name: 'password') this.password,
      @JsonKey(name: 'identification_number') this.identificationNumber,
      @JsonKey(name: 'passport_number') this.passportNumber,
      @JsonKey(name: 'birth_date') this.birthDate,
      @JsonKey(name: 'location') this.location})
      : super._();

  factory _$UnauthorizedUser.fromJson(Map<String, dynamic> json) =>
      _$_$UnauthorizedUserFromJson(json);

  @override
  @JsonKey(name: 'email')
  final String? email;
  @override
  @JsonKey(name: 'first_name')
  final String? firstName;
  @override
  @JsonKey(name: 'last_name')
  final String? lastName;
  @override
  @JsonKey(name: 'password')
  final String? password;
  @override
  @JsonKey(name: 'identification_number')
  final int? identificationNumber;
  @override
  @JsonKey(name: 'passport_number')
  final int? passportNumber;
  @override
  @JsonKey(name: 'birth_date')
  final DateTime? birthDate;
  @override
  @JsonKey(name: 'location')
  final Location? location;

  @override
  String toString() {
    return 'User.unauthorized(email: $email, firstName: $firstName, lastName: $lastName, password: $password, identificationNumber: $identificationNumber, passportNumber: $passportNumber, birthDate: $birthDate, location: $location)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is UnauthorizedUser &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.firstName, firstName) ||
                const DeepCollectionEquality()
                    .equals(other.firstName, firstName)) &&
            (identical(other.lastName, lastName) ||
                const DeepCollectionEquality()
                    .equals(other.lastName, lastName)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.identificationNumber, identificationNumber) ||
                const DeepCollectionEquality().equals(
                    other.identificationNumber, identificationNumber)) &&
            (identical(other.passportNumber, passportNumber) ||
                const DeepCollectionEquality()
                    .equals(other.passportNumber, passportNumber)) &&
            (identical(other.birthDate, birthDate) ||
                const DeepCollectionEquality()
                    .equals(other.birthDate, birthDate)) &&
            (identical(other.location, location) ||
                const DeepCollectionEquality()
                    .equals(other.location, location)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(firstName) ^
      const DeepCollectionEquality().hash(lastName) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(identificationNumber) ^
      const DeepCollectionEquality().hash(passportNumber) ^
      const DeepCollectionEquality().hash(birthDate) ^
      const DeepCollectionEquality().hash(location);

  @JsonKey(ignore: true)
  @override
  $UnauthorizedUserCopyWith<UnauthorizedUser> get copyWith =>
      _$UnauthorizedUserCopyWithImpl<UnauthorizedUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String token) authorized,
    required TResult Function(
            @JsonKey(name: 'email') String? email,
            @JsonKey(name: 'first_name') String? firstName,
            @JsonKey(name: 'last_name') String? lastName,
            @JsonKey(name: 'password') String? password,
            @JsonKey(name: 'identification_number') int? identificationNumber,
            @JsonKey(name: 'passport_number') int? passportNumber,
            @JsonKey(name: 'birth_date') DateTime? birthDate,
            @JsonKey(name: 'location') Location? location)
        unauthorized,
  }) {
    return unauthorized(email, firstName, lastName, password,
        identificationNumber, passportNumber, birthDate, location);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String token)? authorized,
    TResult Function(
            @JsonKey(name: 'email') String? email,
            @JsonKey(name: 'first_name') String? firstName,
            @JsonKey(name: 'last_name') String? lastName,
            @JsonKey(name: 'password') String? password,
            @JsonKey(name: 'identification_number') int? identificationNumber,
            @JsonKey(name: 'passport_number') int? passportNumber,
            @JsonKey(name: 'birth_date') DateTime? birthDate,
            @JsonKey(name: 'location') Location? location)?
        unauthorized,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(email, firstName, lastName, password,
          identificationNumber, passportNumber, birthDate, location);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AuthorizedUser value) authorized,
    required TResult Function(UnauthorizedUser value) unauthorized,
  }) {
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AuthorizedUser value)? authorized,
    TResult Function(UnauthorizedUser value)? unauthorized,
    required TResult orElse(),
  }) {
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$UnauthorizedUserToJson(this)..['runtimeType'] = 'unauthorized';
  }
}

abstract class UnauthorizedUser extends User {
  const factory UnauthorizedUser(
      {@JsonKey(name: 'email') String? email,
      @JsonKey(name: 'first_name') String? firstName,
      @JsonKey(name: 'last_name') String? lastName,
      @JsonKey(name: 'password') String? password,
      @JsonKey(name: 'identification_number') int? identificationNumber,
      @JsonKey(name: 'passport_number') int? passportNumber,
      @JsonKey(name: 'birth_date') DateTime? birthDate,
      @JsonKey(name: 'location') Location? location}) = _$UnauthorizedUser;
  const UnauthorizedUser._() : super._();

  factory UnauthorizedUser.fromJson(Map<String, dynamic> json) =
      _$UnauthorizedUser.fromJson;

  @JsonKey(name: 'email')
  String? get email => throw _privateConstructorUsedError;
  @JsonKey(name: 'first_name')
  String? get firstName => throw _privateConstructorUsedError;
  @JsonKey(name: 'last_name')
  String? get lastName => throw _privateConstructorUsedError;
  @JsonKey(name: 'password')
  String? get password => throw _privateConstructorUsedError;
  @JsonKey(name: 'identification_number')
  int? get identificationNumber => throw _privateConstructorUsedError;
  @JsonKey(name: 'passport_number')
  int? get passportNumber => throw _privateConstructorUsedError;
  @JsonKey(name: 'birth_date')
  DateTime? get birthDate => throw _privateConstructorUsedError;
  @JsonKey(name: 'location')
  Location? get location => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UnauthorizedUserCopyWith<UnauthorizedUser> get copyWith =>
      throw _privateConstructorUsedError;
}

Location _$LocationFromJson(Map<String, dynamic> json) {
  return _Location.fromJson(json);
}

/// @nodoc
class _$LocationTearOff {
  const _$LocationTearOff();

  _Location call(String region, String city) {
    return _Location(
      region,
      city,
    );
  }

  Location fromJson(Map<String, Object> json) {
    return Location.fromJson(json);
  }
}

/// @nodoc
const $Location = _$LocationTearOff();

/// @nodoc
mixin _$Location {
  String get region => throw _privateConstructorUsedError;
  String get city => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LocationCopyWith<Location> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocationCopyWith<$Res> {
  factory $LocationCopyWith(Location value, $Res Function(Location) then) =
      _$LocationCopyWithImpl<$Res>;
  $Res call({String region, String city});
}

/// @nodoc
class _$LocationCopyWithImpl<$Res> implements $LocationCopyWith<$Res> {
  _$LocationCopyWithImpl(this._value, this._then);

  final Location _value;
  // ignore: unused_field
  final $Res Function(Location) _then;

  @override
  $Res call({
    Object? region = freezed,
    Object? city = freezed,
  }) {
    return _then(_value.copyWith(
      region: region == freezed
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$LocationCopyWith<$Res> implements $LocationCopyWith<$Res> {
  factory _$LocationCopyWith(_Location value, $Res Function(_Location) then) =
      __$LocationCopyWithImpl<$Res>;
  @override
  $Res call({String region, String city});
}

/// @nodoc
class __$LocationCopyWithImpl<$Res> extends _$LocationCopyWithImpl<$Res>
    implements _$LocationCopyWith<$Res> {
  __$LocationCopyWithImpl(_Location _value, $Res Function(_Location) _then)
      : super(_value, (v) => _then(v as _Location));

  @override
  _Location get _value => super._value as _Location;

  @override
  $Res call({
    Object? region = freezed,
    Object? city = freezed,
  }) {
    return _then(_Location(
      region == freezed
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String,
      city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Location implements _Location {
  const _$_Location(this.region, this.city);

  factory _$_Location.fromJson(Map<String, dynamic> json) =>
      _$_$_LocationFromJson(json);

  @override
  final String region;
  @override
  final String city;

  @override
  String toString() {
    return 'Location(region: $region, city: $city)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Location &&
            (identical(other.region, region) ||
                const DeepCollectionEquality().equals(other.region, region)) &&
            (identical(other.city, city) ||
                const DeepCollectionEquality().equals(other.city, city)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(region) ^
      const DeepCollectionEquality().hash(city);

  @JsonKey(ignore: true)
  @override
  _$LocationCopyWith<_Location> get copyWith =>
      __$LocationCopyWithImpl<_Location>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_LocationToJson(this);
  }
}

abstract class _Location implements Location {
  const factory _Location(String region, String city) = _$_Location;

  factory _Location.fromJson(Map<String, dynamic> json) = _$_Location.fromJson;

  @override
  String get region => throw _privateConstructorUsedError;
  @override
  String get city => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$LocationCopyWith<_Location> get copyWith =>
      throw _privateConstructorUsedError;
}
