// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AuthorizedUser _$_$AuthorizedUserFromJson(Map<String, dynamic> json) {
  return _$AuthorizedUser(
    json['token'] as String,
  );
}

Map<String, dynamic> _$_$AuthorizedUserToJson(_$AuthorizedUser instance) =>
    <String, dynamic>{
      'token': instance.token,
    };

_$UnauthorizedUser _$_$UnauthorizedUserFromJson(Map<String, dynamic> json) {
  return _$UnauthorizedUser(
    email: json['email'] as String?,
    firstName: json['first_name'] as String?,
    lastName: json['last_name'] as String?,
    password: json['password'] as String?,
    identificationNumber: json['identification_number'] as int?,
    passportNumber: json['passport_number'] as int?,
    birthDate: json['birth_date'] == null
        ? null
        : DateTime.parse(json['birth_date'] as String),
    location: json['location'] == null
        ? null
        : Location.fromJson(json['location'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$_$UnauthorizedUserToJson(_$UnauthorizedUser instance) =>
    <String, dynamic>{
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'password': instance.password,
      'identification_number': instance.identificationNumber,
      'passport_number': instance.passportNumber,
      'birth_date': instance.birthDate?.toIso8601String(),
      'location': instance.location,
    };

_$_Location _$_$_LocationFromJson(Map<String, dynamic> json) {
  return _$_Location(
    json['region'] as String,
    json['city'] as String,
  );
}

Map<String, dynamic> _$_$_LocationToJson(_$_Location instance) =>
    <String, dynamic>{
      'region': instance.region,
      'city': instance.city,
    };
