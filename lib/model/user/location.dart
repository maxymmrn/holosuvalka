part of 'user.dart';

@freezed
class Location with _$Location {
  const factory Location(String region, String city) = _Location;

  factory Location.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$LocationFromJson(json);
}
